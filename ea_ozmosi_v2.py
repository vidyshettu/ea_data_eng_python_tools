import json
import pandas as pd
from pprint import pprint
import fastparquet
import re
import boto3
import os

 
def get_parent_key(inputDict):

    allKeysHere = list(inputDict.items())

    k = list()
    v = list()

    for parentKey, childKey in allKeysHere:

        #if isinstance(inputDict(parentKey), dict):
        if isinstance(inputDict[parentKey], dict):

            sub_k, sub_v  = get_parent_key(childKey)

            dottedColKeys = ['{}.{}'.format(parentKey, dictkey) for dictkey in sub_k]
            dottedColVals = sub_v

            k += dottedColKeys
            v += dottedColVals

        #elif isinstance(inputDict(parentKey), list):
        #    pprint("ignoring list..")

        else:

            k += [parentKey]
            v += [childKey]

    return k, v

def generate_parq(df, chunk_num):
    parq_filename = f"ozmosi_{chunk_num}.parq"
    local_tmp_file = f"/tmp/{parq_filename}"
    df.to_parquet(local_tmp_file, engine='auto', compression="gzip")

    s3cli = boto3.client('s3')
    bucket = 'ea-dmz-us-east-1-landing'
    prefix = 'ea-dmz-exp/ozmosi/parq/level1/'

    s3cli.upload_file(local_tmp_file, bucket, prefix + parq_filename)

    os.remove(f"/tmp/{parq_filename}")

def read_json(file_path, chunk_size, max_rows):
    err_no = 0

    with open(file_path, 'r') as file:
        end = chunk_size
        chunk_num = 0

        while end <= max_rows:

            chunk_num += 1
            df_master = pd.DataFrame()

            try:

                chunk = [json.loads(file.readline().replace('\\n', '\xfe')) for i in range(end-chunk_size, end)]

                for each_element in chunk:

                    chunk_dict = dict(chunk[0])
                    k, v = get_parent_key(chunk_dict)

                    #remove special characters as they're not supported for headers in athena
                    k = [re.sub(r"[-_$()\"#/@;:<>{}`+=~|!?,]", "", header) for header in k]

                    result_dict = dict(zip(k, v))

                    df_temp = pd.DataFrame([result_dict], columns=result_dict.keys())
                    #df_master = pd.concat([df_master, df], axis=0).reset_index()
                    df_master = pd.concat([df_master, df_temp])

            except json.decoder.JSONDecodeError as e:
                raise


            generate_parq(df_master, chunk_num)
            print(f"completed chunk number {chunk_num}")
            #df_master.to_csv("/Users/vidyshettu/osz.csv", index=False, header=True)

            end += chunk_size
            #print(f'Chunk Num: {chunk_num} and {end} rows processed')

        return 'Complete'


if __name__ == '__main__':
    raw_file_path = "/Users/vidyshettu/ozmosi/rawdata/ClinicalTrial_point-in-time.json"
    stream = read_json(raw_file_path, 1000, 1092402)
