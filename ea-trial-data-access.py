import boto3
import argparse
import json
from botocore.errorfactory import ClientError
import sys

def CheckVendorName(value):
    ivalue = str(value)

    if len(ivalue) > 64:
        raise argparse.ArgumentTypeError("%s is an invalid value. Vendor name cannot be more than 64 digits." % value)

    if len(ivalue.split()) > 5:
        raise argparse.ArgumentTypeError("%s is an invalid value. Vendor name cannot be more than 5 words." % value)

    return ivalue

def CheckVendorAcctNumber(value):
    ivalue = int(value)

    if ivalue <= 0:
        raise argparse.ArgumentTypeError("%s is an invalid aws account number. Account number cannot be in negative or zero." % value)

    if len(str(ivalue)) != 12:
        raise argparse.ArgumentTypeError("%s is an invalid aws account number. It's a 12 digit number." % value)

    return ivalue

def CheckEAAcct(value):
    ivalue = str(value)

    if ivalue.lower() != "stg" and ivalue.lower() != "dmz":
        raise argparse.ArgumentTypeError("%s is an invalid EA account. It must be either DMZ or STG." % value)

    return ivalue

def get_ea_acct_no(ea_arg):
    if ea_arg == 'dmz':
        return '814090745415'
    else:
        return '877418987012'

def create_folder_dmz(vendor_name, s3cli):
    bucket_name = "ea-dmz-trial-data"

    # Check if folder already exists
    try:
        s3cli.head_object(Bucket=bucket_name, Key=vendor_name+"/")
        print('Program Failed. S3 bucket already available for this vendor.')
        sys.exit(1)

    except ClientError:
        s3cli.put_object(Bucket=bucket_name, Key=vendor_name+"/")

    
def create_accesspoints(vendor_name, ea_acct_no, s3ctrlcli):
    try:
        s3ctrlcli.create_access_point(AccountId=ea_acct_no,
                                  Name="ea-trial-data-" + vendor_name,
                                  Bucket="ea-dmz-trial-data",
                                  PublicAccessBlockConfiguration={
                                      'BlockPublicAcls': True,
                                      'IgnorePublicAcls': True,
                                      'BlockPublicPolicy': True,
                                      'RestrictPublicBuckets': True
                                  }
                                  )

    except:
        print('Program Failed. Unable to create s3 accesspoints.')
        raise


def generate_ap_policy(vendor_name, vendor_acct_no, ea_acct_no, s3ctrlcli):
    ap_policy = {
        'Version': '2012-10-17',
        'Statement': [
            {
                'Effect': 'Allow',
                'Principal': {
                    'AWS': f'arn:aws:iam::{vendor_acct_no}:root'
                },
                'Action': [
                    's3:GetObject',
                    's3:PutObject',
                    's3:DeleteObject',
                    's3:GetObjectAcl',
                    's3:GetObjectVersion',
                    's3:GetObjectVersionAcl',
                    's3:AbortMultipartUpload',
                    's3:ListMultipartUploadParts'
                ],
                'Resource': f'arn:aws:s3:us-east-1:{ea_acct_no}:accesspoint/ea-trial-data-{vendor_name}/object/{vendor_name}/*'
            },
            {
                'Effect': 'Allow',
                'Principal': {
                    'AWS': f'arn:aws:iam::{vendor_acct_no}:root'
                },
                'Action': 's3:ListBucket',
                'Resource': f'arn:aws:s3:us-east-1:{ea_acct_no}:accesspoint/ea-trial-data-{vendor_name}',
                'Condition': {
                    'StringEquals': {
                        's3:prefix': [
                            '',
                            f'{vendor_name}/',
                            f'{vendor_name}/*'
                        ],
                        's3:delimiter': '/'
                    }
                }
            },
            {
                'Effect': 'Allow',
                'Principal': {
                    'AWS': f'arn:aws:iam::{vendor_acct_no}:root'
                },
                'Action': 's3:ListBucket',
                'Resource': f'arn:aws:s3:us-east-1:{ea_acct_no}:accesspoint/ea-trial-data-{vendor_name}',
                'Condition': {
                    'StringLike': {
                        's3:prefix': [
                            f'{vendor_name}',
                            f'{vendor_name}/*'
                        ]
                    }
                }
            }
        ]
    }

    ap_policy = json.dumps(ap_policy)

    print(ap_policy)

    try:
        s3ctrlcli.put_access_point_policy(
            AccountId=ea_acct_no,
            Name=f'ea-trial-data-{vendor_name}',
            Policy=ap_policy
        )
    except:
        print('Program Failed. Unable to create s3 accesspoint policy.')
        raise

### __main__ ####

''' 
########################################################################################################################################

This script is for trial data to create S3 folder for vendor and also to create accesspoints to provide permissions for Vendor to copy their files. 

It takes three input parameters:
1). vendor name to create s3 folder
2). vendor's 12 digit aws account number. 
3). EA's account where you want to create folder and access points. DMZ or STG  

##########################################################################################################################################
'''

# Collect Input Parameters
parser = argparse.ArgumentParser()
parser.add_argument("VendorName", type=CheckVendorName, help="Add vendor name to create a folder in s3")
parser.add_argument("VendorAcctNumber", type=CheckVendorAcctNumber, help="Add vendor account number to provide S3 aceess using accesspoint")
parser.add_argument("EAAccount", type=CheckEAAcct, help="DMZ or STG?")

args = parser.parse_args()
vendor_name = (args.VendorName).replace(" ","").lower()
vendor_acct_no = str(args.VendorAcctNumber)
ea_acct_no = get_ea_acct_no((args.EAAccount).lower())


# boto3 connection
s3cli = boto3.client("s3")
s3ctrlcli = boto3.client("s3control")

# Create folder in DMZ
# At this moment the program only works for DMZ only.
if (args.EAAccount).lower() == "dmz":
    create_folder_dmz(vendor_name, s3cli)

    # Create access point
    create_accesspoints(vendor_name, ea_acct_no, s3ctrlcli)

    # Generate accesspoint policy
    generate_ap_policy(vendor_name, vendor_acct_no, ea_acct_no, s3ctrlcli)

    print("Program completed. S3 folder and access points created.")